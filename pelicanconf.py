#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'David'
SITENAME = u'vAIR.io Build Log'
SITEURL = 'http://buildlog.vair.io'

PATH = 'content'
OUTPUT_PATH = 'public'

TIMEZONE = 'America/Los_Angeles'

DEFAULT_LANG = u'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None


THEME = '/data/pelican-themes/blue-penguin'
#PLUGIN_PATHS = ['/data/pelican-plugins']
#PLUGINS = ['one', 'two']

# Blogroll
LINKS = (('Pelican', 'http://getpelican.com/'),
         ('Python.org', 'http://python.org/'),
         ('Jinja2', 'http://jinja.pocoo.org/'),
         ('You can modify those links in your config file', '#'),)

# Social widget
SOCIAL = (('You can add links in your config file', '#'),
          ('Another social link', '#'),)

DEFAULT_PAGINATION = 5

# Mostly this is just for Let's Encrypt.
STATIC_PATHS = ['images',
                'extra/Tp7ABlzmZ7rGuj0umfdgc_Ze-bNiOplxY9p7VvlieRE',
                'extra/2018AODNSBL.txt']

EXTRA_PATH_METADATA = {
        'extra/Tp7ABlzmZ7rGuj0umfdgc_Ze-bNiOplxY9p7VvlieRE': {'path': '.well-known/acme-challenge/Tp7ABlzmZ7rGuj0umfdgc_Ze-bNiOplxY9p7VvlieRE'},
        'extra/2018AODNSBL.txt': {'path': '2018AODNSBL.txt'},
}
# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True
